from http.server import BaseHTTPRequestHandler, HTTPServer
import time
import os

hostName = "localhost"
hostPort = 9000

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

        if self.path == '/poweroff' or self.path == '/poweroff/':
            self.wfile.write(bytes('<html>', "utf-8"))
            self.wfile.write(bytes('  <head>', "utf-8"))
            self.wfile.write(bytes('    <title>Power off</title>', "utf-8"))
            self.wfile.write(bytes('  </head>', "utf-8"))
            self.wfile.write(bytes('  <body>', "utf-8"))
            self.wfile.write(bytes('Poweroff successfull.', "utf-8"))
            self.wfile.write(bytes('  </body>', "utf-8"))
            self.wfile.write(bytes('</html>', "utf-8"))
            os.system("sudo shutdown now -h")
        elif self.path == '/reboot' or self.path == '/reboot/':
            self.wfile.write(bytes('<html>', "utf-8"))
            self.wfile.write(bytes('  <head>', "utf-8"))
            self.wfile.write(bytes('    <title>Reboot</title>', "utf-8"))
            self.wfile.write(bytes('  </head>', "utf-8"))
            self.wfile.write(bytes('  <body>', "utf-8"))
            self.wfile.write(bytes('Reboot successfull.', "utf-8"))
            self.wfile.write(bytes('  </body>', "utf-8"))
            self.wfile.write(bytes('</html>', "utf-8"))
            os.system("sudo reboot")
        else:
            self.wfile.write(bytes("<html><head><title>Title goes here.</title></head>", 
"utf-8"))
        self.wfile.write(bytes("<body><p>This is a test.</p>", "utf-8"))
        self.wfile.write(bytes("<p>You accessed path: %s</p>" % self.path, "utf-8"))
        self.wfile.write(bytes("</body></html>", "utf-8"))

myServer = HTTPServer(('', hostPort), MyServer)
print(time.asctime(), "Server Starts - %s:%s" % (hostName, hostPort))

try:
    myServer.serve_forever()
except KeyboardInterrupt:
    pass

myServer.server_close()
print(time.asctime(), "Server Stops - %s:%s" % (hostName, hostPort))
